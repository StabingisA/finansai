package lt.projektas.finansai;

import java.util.ArrayList;

public class Balansas {

	private ArrayList<Irasas> irasai = new ArrayList<Irasas>();
	private double pajamos;

	public void bendrosIslaidos() {
		System.out.println(pajamos);
	}

	public void sukurtiIrasa(int id, int kategorija, double suma, String bankoKorta, String papildInf) {
		Irasas irasas = new Irasas(++id, kategorija, suma, bankoKorta, papildInf);

		irasai.add(irasas);

		System.out.println(irasas.toString());
		pajamos += suma;
	}

	public void print() {
		for (int i = 0; i < irasai.size(); i++) {
			System.out.println(irasai.get(i).toString());
		}
	}

	public int id() {
		return irasai.size();
	}
}
