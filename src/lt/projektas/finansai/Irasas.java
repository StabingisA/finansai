package lt.projektas.finansai;


import java.util.Date;

public class Irasas {

	private int id;
	private int kategorija;
	private double suma;
	private String data;
	private boolean arISask;
	private String arBankoKorta;
	private String papildInfo;



	public Irasas(int id, int kategorija, double suma, String arBankoKorta, String papildInfo) {
		Date date = new Date();
		this.id = id;
		this.kategorija = kategorija;
		this.suma = suma;
		this.arISask = true;
		this.data = date.toString();
		this.arBankoKorta = arBankoKorta;
		this.papildInfo = papildInfo;


	}


	public String toString() {
		return "Irasas {" +
				" Id:" + id +
				", Kategorija " + kategorija +
				", Suma:  " + suma +
				", Data: " + data + '\'' +
				", Ar saskaitoj: " + arISask +
				", Atsiskaitymo budas: " + arBankoKorta +
				", Papildoma informacija: " + papildInfo + '\'' +
				'}';
	}

}