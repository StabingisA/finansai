package lt.projektas.finansai;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Scanner;

public class PagrindinisMeniu {
	DateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd HH:mm:ss");
	Balansas bendrasuma = new Balansas();

	public void pagrMeniu() {
		Scanner scan = new Scanner(System.in);
		System.out.println("Pasirinkite kurią funkciją norite atlikti:");
		System.out.println("1.-Pajamų įvedimas-");
		System.out.println("2.-Išlaidų įvedimas-");
		System.out.println("3.-Balansas-");
		System.out.println("4.-Pajamų/Išlaidų informacija-");
		System.out.println("5.-Baigti darbą-");
		int kategorija = scan.nextInt();
		if (kategorija == 1) {
			tekstai(kategorija);
			int pasirinkimas = scan.nextInt();
			if (pasirinkimas == 0) {
				pagrMeniu();
			}
			System.out.println("Iveskite suma:");
			int suma = scan.nextInt();
			System.out.println("pasirinkite mokejimo buda: ");
			String budas = scan.next();
			System.out.println("Ar norite dar ka nors prideti?");
			String papildINfo = scan.next();
			bendrasuma.sukurtiIrasa(bendrasuma.id(), pasirinkimas, suma, budas, papildINfo);

		} else if (kategorija == 2) {
			tekstai(kategorija);
			int pasirinkimas = scan.nextInt();
			if (pasirinkimas == 0) {
				pagrMeniu();
			}
			System.out.println("Iveskite suma:");
			int suma = -scan.nextInt();
			System.out.println("pasirinkite mokejimo buda: ");
			String budas = scan.next();
			System.out.println("Ar norite dar ka nors prideti?");
			String papildINfo = scan.next();
			bendrasuma.sukurtiIrasa(bendrasuma.id(), pasirinkimas, suma, budas, papildINfo);

		} else if (kategorija == 3) {
			bendrasuma.bendrosIslaidos();
		} else if (kategorija == 4) {
			bendrasuma.print();
		} else if (kategorija == 5) {
			System.out.println("Pasirinkote baigti darbą.\nPrograma išsijungia...........");
			scan.close();
			System.exit(kategorija);
		} else if (kategorija == 0) {
			tekstai(0);

		} else
			System.out.println("Užklausos nesupratau. Pasirinkite dar kart.");
		pagrMeniu();

	}

	private void tekstai(int i) {

		if (i == 1) {
			System.out.println("Jus pasirinkote -Islaidos-");
			System.out.println("1.-Prekybos centras-");
			System.out.println("2.-Lizingo mokesciai-");
			System.out.println("3.-Busto mokesciai-");
			System.out.println("4.-Mokslai-");
			System.out.println("5.-Pramogos-");
			System.out.println("6.-Automobilis-");
			System.out.println("7.-Apranga-");
			System.out.println("8.-Sveikata-");
			System.out.println("9.-Kita-");
			System.out.println("0.-Grizti atgal-");
		} else if (i == 2) {
			System.out.println("Jus pasirinkote -Pajamos-");
			System.out.println("1.-Alga-");
			System.out.println("2.-NT nuoma-");
			System.out.println("3.-Stipendija-");
			System.out.println("4.-Pasalpa-");
			System.out.println("5.-Individuali veikla-");
			System.out.println("6.-Dovana-");
			System.out.println("7.-Kita-");
			System.out.println("0.-Grizti atgal-");
		} else if (i == 3) {

		} else if (i == 4) {

		} else if (i == 5) {

		}

	}

}
